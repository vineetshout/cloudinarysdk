<?php
require 'autoload.php';
require 'src/Helpers.php'; 
require 'src/Api/Response.php';

Cloudinary::config(array( 
  "cloud_name" => "byosocial", 
  "api_key" => "611564535592381", 
  "api_secret" => "ceo_QRKlwVkA7Q3gsbUeiyUBBkg", 
  "secure" => true
));

// Initialise a Search API object
$search = new \Cloudinary\Search();

// Here we will store all public_ids retrieved based on the expression in our Live run
$public_id_list = [];

/*
Debug - Let's do a test run to find out how many resources the query we will use will match. We can use max_results=>0 since we only want the base response.
*/

$res = $search->expression('type:upload AND resource_type:image AND created_at>2020-02-01')->max_results(0)->execute();

echo "Total Count is: " . $res["total_count"] . "\n";

/*
Let's start the actual calls to retrieve the public_ids - created_at field is used to filter - Timestamp is YYYY-MM-DD. max_results=500 to retrieve as many as possible with a$
*/

$result = $search->expression('type:upload AND resource_type:image AND created_at>2020-02-01')->max_results(500)->execute();

// Merge the first set of results
$public_id_list = array_merge($public_id_list, $result["resources"]);

/*
For debugging, we can loop over the first API call and 500 results to print the public_id and created_at time so we can verify the expected resources are returned (just as a $
*/

// foreach($result["resources"] as $resource) {
//   echo $resource["public_id"] . " - " . $resource["created_at"] . "\n";
// }

/* I have commented this out for now but you will want to uncomment and run that to retrieve all resources. It will loop over each $result until we paginate all results using$
*/

$result["next_cursor"] = "3df158257c7692b769347fca3e826d39206bd9621ceee4dbecbc437c5003c8fa5ed70cfee874559c6ebc8767af66ed63";

while (isset($result["next_cursor"])) {
    $result = $search->expression('type:upload AND resource_type:image AND created_at>2020-02-01')->max_results(500)->next_cursor($result["next_cursor"])->execute();

foreach($result["resources"] as $public_id) {

    $explicit_result = \Cloudinary\Uploader::explicit(
        $public_id['public_id'],
        array(
          "resource_type" => "image",
          "type" => "upload",
          "eager_async" => true,
          "overwrite" => false,
          "eager" => array(
            array("crop" => "scale", "quality" => "auto:low", "width" => 800, "format" => "jpg"),
            array("quality" => "auto:low", "format" => "jpg"),
          )
        )
    );

    // Keep adding the retrieved resources to our list
    // $public_id_list = array_merge($public_id_list, $result["resources"]);
}


}


// Once you have paginated all next_cursors the $public_id_list will contain a list of public_id strings with all matched resources. Use that list to loop over and call Expli$

?>